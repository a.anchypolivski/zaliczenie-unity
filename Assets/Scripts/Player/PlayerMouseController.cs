using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouseController : MonoBehaviour
{
    public Camera cam;
    private float xRotation = 0f;

    public float xSensetivity = 30f;
    public float ySensetivity = 30f;
    
    public void ProcessLook(Vector2 Input)
    {
        float mouseX = Input.x;
        float mouseY = Input.y;

        //calculate camera rotation to look up and down
        xRotation -= (mouseY * Time.deltaTime) * ySensetivity;
        xRotation = Mathf.Clamp(xRotation, -80f, 80f);

        //apply this to our camera transform
        cam.transform.localRotation = Quaternion.Euler(xRotation, 0, 0);

        //rotate player to look right and left
        transform.Rotate(Vector3.up * (mouseX * Time.deltaTime) * xSensetivity);
    
    }
}
