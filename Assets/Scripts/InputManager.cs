using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{

    private PlayerInput playerInput;
    public PlayerInput.OnFootActions onFoot;
    public PlayerInput.ShootingActions shooting;
    public PlayerInput.ReloadActions reload;

    private PlayerController control;
    private PlayerMouseController look;

    // Start is called before the first frame update
    void Awake()
    {
        playerInput = new PlayerInput();
        onFoot = playerInput.OnFoot;

        control = GetComponent<PlayerController>();
        look = GetComponent<PlayerMouseController>();
        onFoot.Jump.performed += ctx => control.Jump();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        control.ProcessMove(onFoot.Movement.ReadValue<Vector2>());
    }
    private void LateUpdate()
    {
        look.ProcessLook(onFoot.Look.ReadValue<Vector2>());
    }
    private void OnEnable()
    {
        onFoot.Enable();
    }
    private void OnDisable()
    {
        onFoot.Disable();
    }
}
